<?php


namespace App\Filters;

abstract class Transformer {

    public function transformCollections(array $items) {
        
        return array_map([$this, 'transform'], $items);
    }


    public abstract function transform($item);
}