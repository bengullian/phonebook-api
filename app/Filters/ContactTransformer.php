<?php

namespace App\Filters;

class ContactTransformer extends Transformer {

    public function transform($contact) {

        return [
            'name' => $contact->name,
            'phone' => $contact->phone,
            'e-mail' => $contact->email
        ];
    }
}