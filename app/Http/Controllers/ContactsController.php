<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use App\Filters\ContactTransformer;
use Illuminate\Http\Request;
use App\Contact;
use App\User;

class ContactsController extends ApiController
{

    protected $contactTransformer;

    function __construct(ContactTransformer $contactTransformer) 
    {   
        $this->contactTransformer = $contactTransformer;        
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Auth::user()->contacts; 
        return $this->respond([
            'data' => $this->contactTransformer->transformCollections($contacts->all())
        ]);
    }



    /**
     *  create a new resource using the store method.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($this->store($request)) {
            return redirect('home');
        }
    }



    /**
     * Store a newly created resource in storage.
     * bail at first validation failure
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $validated = $request->validate([
            'name' => 'bail|required|max:25',
            'phone' => 'required',
            'email' => 'sometimes|required|email',
        ]);
        
        $contact = new Contact($request->all());
        Auth::user()->contacts()->save($contact);

        if ($contact) {
            return $this->respond([
                'response' => 'contact created successfully',
                'created' => true
            ]);
        }
   
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {  
        $contact = Contact::where('id', '=', $id)
                    ->where('user_id', '=', Auth::id())
                    ->first();     

        if (!$contact) {
            return $this->respondNotFound('Contact does not exist'); 
        }

        return $this->respond([
            'data' => $this->contactTransformer->transform($contact)            
        ],['found' => true]);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contact = Contact::where('id', '=', $id)
                    ->where('user_id', '=', Auth::id())
                    ->first(); 

        if (!$contact) {
            return $this->respondNotFound('Contact does not exist');   
        }

        $updates = [];
        
        if ($request->name) {
            $updates['name'] = $request->input('name');
        }

        if ($request->phone) {
            $updates['phone'] = $request->input('phone');
        }

        if ($request->email) {
            $updates['email'] = $request->input('email');
        }
   
        $contact->update($updates);

        return $this->respond([
            'response' => 'contact '.$request->input('name').' details updated successfully'
        ]);
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = Contact::where('id', '=', $id)
                    ->where('user_id', '=', Auth::id())
                    ->first();

        if (!$contact) {
            return $this->respondNotFound('Contact does not exist');   
        }

        $contact->delete();

        return $this->respond([
            'response' => 'contact deleted successfully'
        ]);
    }
}
