<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Contact;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 30) as $index) {

            Contact::create([
                'name' => $faker->name,
                'user_id' => 1,
                'phone' => $faker->phoneNumber,
                'email' => $faker->email,                
            ]);

        }
    }
}
