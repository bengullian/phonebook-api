@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-8" style="height: 600px; overflow-y:scroll;">
            @include('components/phonebook/contactList')  
        </div>
        <div class="col-sm-4">
            @include('components/phonebook/createContactForm')
        </div>
    </div>
</div>
@endsection
