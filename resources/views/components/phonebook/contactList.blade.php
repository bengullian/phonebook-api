<div class="col-md-12">
            <div class="card">
                <div class="card-header"><h4>My Contacts</h4></div>
            </div>            
            <ul class="list-group list-group-item-action">
                @foreach ($contacts as $contact)
                <li class="list-group-item justify-content-between">
                   <h4>{{ $contact->name }}</h4> 
                    <span class="badge badge-default badge-pill"><h6>{{ $contact->phone }}</h6></span>
                    <span class="badge badge-default badge-pill"><h6>{{ $contact->email }}</h6></span>
                </li>
                @endforeach                      
            </ul>
</div>