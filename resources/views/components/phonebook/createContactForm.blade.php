


<div class="card col-md-12 col-md-offset-2" style="background:#f5f5f0">
<p><h3>Create new Contact</h3></p>
<form method="POST" action="/contacts">
    @csrf
    <div class="form-group">
        <label for="nameField">Name</label>
        <input type="text" class="form-control" id="nameField" name="name" placeholder="Name">
  </div>
  <div class="form-group">
    <label for="phoneField">Phone</label>
    <input type="text" class="form-control" id="phoneField" name="phone" placeholder="Phone">
  </div>
  <div class="form-group">
    <label for="emailField"><E-mail></E-mail></label>
    <input type="text" class="form-control" id="emailField" name="email" placeholder="E-mail">
  </div>

    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-btn fa-user"></i> Submit
            </button>
        </div>
    </div>
</form>
</div>

